breeze-gtk (6.3.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.3.1).
  * New upstream release (6.3.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 28 Feb 2025 00:53:41 +0100

breeze-gtk (6.3.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.3.0).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 10 Feb 2025 15:00:10 +0100

breeze-gtk (6.2.91-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.91).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 23 Jan 2025 23:53:33 +0100

breeze-gtk (6.2.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.90).
  * Bump inter-plasma versioned dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 12 Jan 2025 00:47:07 +0100

breeze-gtk (6.2.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.5).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 05 Jan 2025 11:23:00 +0100

breeze-gtk (6.2.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 03 Dec 2024 16:37:22 +0100

breeze-gtk (6.2.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.3).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 23 Nov 2024 21:57:33 +0100

breeze-gtk (6.2.1-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.1).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 15 Oct 2024 18:23:32 +0200

breeze-gtk (6.2.0-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.90).
  * New upstream release (6.2.0).
  * Tighten inter-Plasma package dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 05 Oct 2024 23:17:48 +0200

breeze-gtk (6.1.5-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.5).
  * Tighten Plasma packages inter-dependencies.
  * Drop useless ${shlibs:Depends} for breeze-gtk-theme.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 11 Sep 2024 23:28:08 +0200

breeze-gtk (6.1.4-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 11 Aug 2024 23:58:24 +0200

breeze-gtk (6.1.3-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.3).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 21 Jul 2024 23:46:19 +0200

breeze-gtk (6.1.0-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.1.0).
  * Update build-deps and deps with the info from cmake.
  * Remove transitional package gtk3-engines-breeze as it has been a
    transitional package for more than 2 Debian releases.

 -- Patrick Franz <deltaone@debian.org>  Thu, 27 Jun 2024 00:17:54 +0200

breeze-gtk (5.27.11-1) unstable; urgency=medium

  [ Patrick Franz ]
  * Bump Standards-Version to 4.7.0 (No changes needed).
  * New upstream release (5.27.11).
  * Update build-deps and deps with the info from cmake.
  * Remove inactive uploaders, thanks for your work.

 -- Patrick Franz <deltaone@debian.org>  Sun, 19 May 2024 12:22:10 +0200

breeze-gtk (5.27.10-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.10).

 -- Patrick Franz <deltaone@debian.org>  Thu, 11 Jan 2024 23:25:27 +0100

breeze-gtk (5.27.9-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.9).

 -- Patrick Franz <deltaone@debian.org>  Fri, 27 Oct 2023 21:54:32 +0200

breeze-gtk (5.27.8-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.8).

 -- Patrick Franz <deltaone@debian.org>  Wed, 13 Sep 2023 22:10:19 +0200

breeze-gtk (5.27.7-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.7).
  * Update build-deps and deps with the info from cmake.

 -- Patrick Franz <deltaone@debian.org>  Thu, 03 Aug 2023 18:55:05 +0200

breeze-gtk (5.27.5-2) unstable; urgency=medium

  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 27 May 2023 18:23:46 +0200

breeze-gtk (5.27.5-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.5).

 -- Patrick Franz <deltaone@debian.org>  Tue, 09 May 2023 23:28:45 +0200

breeze-gtk (5.27.3-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.3).

 -- Patrick Franz <deltaone@debian.org>  Wed, 22 Mar 2023 23:19:08 +0100

breeze-gtk (5.27.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.27.1).
  * New upstream release (5.27.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 28 Feb 2023 15:01:06 +0100

breeze-gtk (5.27.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.27.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 18 Feb 2023 17:08:45 +0100

breeze-gtk (5.26.90-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.90).
  * Bump Standards-Version to 4.6.2, no change required.
  * Update cross-plasma versioned dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 23 Jan 2023 21:50:58 +0100

breeze-gtk (5.26.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 07 Jan 2023 00:22:15 +0100

breeze-gtk (5.26.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 29 Nov 2022 16:06:37 +0100

breeze-gtk (5.26.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.3).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 08 Nov 2022 15:46:41 +0100

breeze-gtk (5.26.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.1).
  * New upstream release (5.26.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 27 Oct 2022 23:35:36 +0200

breeze-gtk (5.26.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.0).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 11 Oct 2022 15:44:23 +0200

breeze-gtk (5.25.90-2) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * Tighten cross-plasma components dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 07 Oct 2022 11:47:04 +0200

breeze-gtk (5.25.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.90).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 25 Sep 2022 00:14:25 +0200

breeze-gtk (5.25.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 09 Sep 2022 23:22:12 +0200

breeze-gtk (5.25.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 02 Aug 2022 17:34:19 +0200

breeze-gtk (5.25.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.2).
  * New upstream release (5.25.3).
  * Release to unstable

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 17 Jul 2022 15:29:08 +0200

breeze-gtk (5.25.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.25.1).
  * Tighten inter-Plasma dependencies.

 -- Patrick Franz <deltaone@debian.org>  Tue, 21 Jun 2022 21:47:29 +0200

breeze-gtk (5.25.0-1) experimental; urgency=medium

  * New upstream release (5.25.0).
  * Tighten cross-plasma components dependencies.
  * Bump Standards-Version to 4.6.1, no change required.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 14 Jun 2022 21:26:55 +0200

breeze-gtk (5.24.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.90).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 20 May 2022 11:46:03 +0200

breeze-gtk (5.24.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 12 May 2022 21:40:28 +0200

breeze-gtk (5.24.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 30 Mar 2022 14:04:57 +0200

breeze-gtk (5.24.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.3).
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 10 Mar 2022 08:01:10 +0100

breeze-gtk (5.24.2-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.24.2).
  * Re-export signing key without extra signatures.
  * Update B-Ds.

 -- Patrick Franz <deltaone@debian.org>  Sat, 26 Feb 2022 21:57:40 +0100

breeze-gtk (5.23.5-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.23.5).
  * Update B-Ds.

 -- Patrick Franz <deltaone@debian.org>  Fri, 07 Jan 2022 18:01:07 +0100

breeze-gtk (5.23.4-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.4).

  [ Patrick Franz ]
  * Update B-Ds.

 -- Patrick Franz <deltaone@debian.org>  Thu, 02 Dec 2021 22:25:15 +0100

breeze-gtk (5.23.3-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.3).

 -- Norbert Preining <norbert@preining.info>  Wed, 10 Nov 2021 08:36:50 +0900

breeze-gtk (5.23.2-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.1).
  * New upstream release (5.23.2).

 -- Norbert Preining <norbert@preining.info>  Wed, 03 Nov 2021 22:21:01 +0900

breeze-gtk (5.23.0-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Update upstream signing-key.
  * Tighten build-dependencies.
  * Bump Standards-Version to 4.6.0 (no changes needed).

 -- Patrick Franz <deltaone@debian.org>  Thu, 14 Oct 2021 22:03:17 +0200

breeze-gtk (5.23.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.0).

 -- Norbert Preining <norbert@preining.info>  Thu, 14 Oct 2021 20:13:13 +0900

breeze-gtk (5.21.5-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Release to unstable.

 -- Patrick Franz <deltaone@debian.org>  Tue, 17 Aug 2021 03:46:55 +0200

breeze-gtk (5.21.5-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.5).

 -- Patrick Franz <patfra71@gmail.com>  Fri, 07 May 2021 18:51:59 +0200

breeze-gtk (5.21.4-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.4).

 -- Patrick Franz <patfra71@gmail.com>  Tue, 06 Apr 2021 17:46:11 +0200

breeze-gtk (5.21.3-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.21.3).

 -- Norbert Preining <norbert@preining.info>  Wed, 17 Mar 2021 05:49:40 +0900

breeze-gtk (5.21.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.2).

 -- Norbert Preining <norbert@preining.info>  Wed, 03 Mar 2021 05:33:48 +0900

breeze-gtk (5.21.1-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.21.1).

 -- Norbert Preining <norbert@preining.info>  Wed, 24 Feb 2021 14:36:38 +0900

breeze-gtk (5.21.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.21.0).

 -- Norbert Preining <norbert@preining.info>  Wed, 17 Feb 2021 05:42:25 +0900

breeze-gtk (5.20.5-1) unstable; urgency=medium

  * New upstream release (5.20.5).

 -- Norbert Preining <norbert@preining.info>  Wed, 06 Jan 2021 23:50:50 +0900

breeze-gtk (5.20.4-2) unstable; urgency=medium

  * Release to unstable.

 -- Norbert Preining <norbert@preining.info>  Tue, 22 Dec 2020 11:04:36 +0900

breeze-gtk (5.20.4-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.20.4).
  * Use Debian Qt/KDE as maintainer.

 -- Norbert Preining <norbert@preining.info>  Wed, 09 Dec 2020 14:17:31 +0900

breeze-gtk (5.19.5-3) unstable; urgency=medium

  [ Scarlett Moore ]
  * Update d/upstream/metadata.
   - Remove obsolete entries already defined in d/copyright.
   - Update repository* to invent.kde.org.
   - Update changelog to announcements page.

 -- Norbert Preining <norbert@preining.info>  Fri, 06 Nov 2020 08:37:21 +0900

breeze-gtk (5.19.5-2) experimental; urgency=medium

  * Rebuild for Qt 5.15

 -- Norbert Preining <norbert@preining.info>  Mon, 02 Nov 2020 09:27:38 +0900

breeze-gtk (5.19.5-1) experimental; urgency=medium

  * New upstream release (5.19.5).
  * Add Patrick and myself to uploaders.

 -- Norbert Preining <norbert@preining.info>  Mon, 19 Oct 2020 10:19:03 +0900

breeze-gtk (5.19.4-1) experimental; urgency=medium

  * Team upload.

  [ Norbert Preining ]
  * New upstream release (5.19.4).
  * Remove Max from Uploaders as requested on IRC, add Scarlett.
  * Bump compat level to 13.
  * Enable team builder to be able to build on salsa.
  * Add Rules-Requires-Root field to control.
  * Remove not needed injection of linker flags.
  * Enable build hardening.
  * Update Homepage link to point to new invent.kde.org
  * Update field Source in debian/copyright to invent.kde.org move.
  * Set field Upstream-Contact in debian/copyright.

 -- Norbert Preining <norbert@preining.info>  Fri, 16 Oct 2020 09:37:20 +0900

breeze-gtk (5.17.5-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * Bump Standards-Version to 4.5.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Fri, 14 Feb 2020 21:34:22 +0100

breeze-gtk (5.17.5-1) experimental; urgency=medium

  * Team upload.

  [ Maximiliano Curia ]
  * New upstream release (5.16.5).
  * Salsa CI automatic initialization by Tuco
  * Update build-deps and deps with the info from cmake

  [ Pino Toscano ]
  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - add kde-style-breeze, needed for the color schemes
    - remove unused libkf5archive-dev, libkf5configwidgets-dev, libkf5i18n-dev,
      libkf5iconthemes-dev, libkf5kcmutils-dev, libkf5kio-dev,
      libkf5newstuff-dev, and xvfb
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
  * Simplify installation by forcing the destdir to the installation directory
    of breeze-gtk-theme
    - drop breeze-gtk-theme.install
  * Drop the 'testsuite' autopkgtest, as it does not test the installed
    packages. (Closes: #909138)
  * Bump Standards-Version to 4.4.1, no changes required.
  * Drop dh_auto_test override, no more needed.
  * Improve the dependencies of the transitional gtk3-engines-breeze:
    - depend on the current version of breeze-gtk-theme
    - add ${misc:Depends}
  * Remove duplicated Priority in gtk3-engines-breeze.
  * Use https for Format in copyright.

 -- Pino Toscano <pino@debian.org>  Sat, 18 Jan 2020 23:12:20 +0100

breeze-gtk (5.14.5-1) unstable; urgency=medium

  * New upstream release (5.14.5).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 24 Jan 2019 09:25:39 -0300

breeze-gtk (5.14.3-1) unstable; urgency=medium

  * Update upsteam signing-key
  * New upstream release (5.14.3).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 23 Nov 2018 08:50:24 -0300

breeze-gtk (5.13.5-1) unstable; urgency=medium

  * New upstream release (5.13.5).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 06 Sep 2018 20:40:28 +0200

breeze-gtk (5.13.4-1) unstable; urgency=medium

  * New upstream release (5.13.4).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Sun, 19 Aug 2018 23:17:49 +0200

breeze-gtk (5.13.1-1) unstable; urgency=medium

  * New upstream release (5.12.2).
  * New upstream release (5.13.1).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Tue, 26 Jun 2018 13:42:46 +0200

breeze-gtk (5.12.1-1) sid; urgency=medium

  * Use the salsa canonical urls
  * New upstream release (5.12.1).
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Tue, 20 Feb 2018 22:08:36 +0100

breeze-gtk (5.12.0-2) sid; urgency=medium

  * New revision
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Mon, 12 Feb 2018 16:03:25 +0100

breeze-gtk (5.12.0-1) experimental; urgency=medium

  * Bump debhelper build-dep and compat to 11.
  * Add link options as-needed
  * Add Bhushan Shah upstream signing key
  * New upstream release (5.12.0).
  * Bump Standards-Version to 4.1.3.
  * Use https uri for uscan
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Thu, 08 Feb 2018 15:20:25 +0100

breeze-gtk (5.11.4-1) experimental; urgency=medium

  * New upstream release (5.11.4).
  * Bump to Standards-Version 4.1.2
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Wed, 03 Jan 2018 16:48:42 -0300

breeze-gtk (5.10.5-2) sid; urgency=medium

  * New revision
  * Bump Standards-Version to 4.1.0.
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sun, 03 Sep 2017 09:55:12 +0200

breeze-gtk (5.10.5-1) experimental; urgency=medium

  * New upstream release (5.10.3).
  * Update build-deps and deps with the info from cmake
  * Bump Standards-Version to 4.0.0.
  * Update upstream metadata
  * Rename package to breeze-gtk-theme.
    Thanks to Jeremy Bicha and George B. (Closes: 822975)
  * New upstream release (5.10.4).
  * Update build-deps and deps with the info from cmake
  * Set Priority to optional
  * New upstream release (5.10.5).
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Mon, 28 Aug 2017 15:28:01 +0200

breeze-gtk (5.8.7-1) unstable; urgency=medium

  * New upstream release (5.8.7)
     + Set no stepper button for GTK3 scrollbars on gtk >=3.20
       This is a workaround to mitigate broken (huge wide) scrollbars in
       mozilla/Gecko based applications for GTK3 >= 3.20 in versions 52
       onwards.
       Ref: https://bugs.kde.org/show_bug.cgi?id=377008 and
            https://bugzilla.mozilla.org/show_bug.cgi?id=1343802
       The mozilla bug is now 'fixed' and landing in nightlies v55, but may
       continue to affect earlier releases if not brought forward upstream.
       Regardless of any other concern, applying this brings the appearance
       of newer breeze and breeze-gtk scrollars into closer accord.
       KDE#377008

 -- Maximiliano Curia <maxy@debian.org>  Tue, 06 Jun 2017 17:14:40 +0200

breeze-gtk (5.8.5-1) experimental; urgency=medium

  * New upstream release (5.8.5).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 30 Dec 2016 18:46:12 +0100

breeze-gtk (5.8.4-1) unstable; urgency=medium

  * New upstream release (5.8.4)

 -- Maximiliano Curia <maxy@debian.org>  Wed, 23 Nov 2016 18:27:48 +0100

breeze-gtk (5.8.2-1) unstable; urgency=medium

  * New upstream release (5.8.2)

 -- Maximiliano Curia <maxy@debian.org>  Wed, 19 Oct 2016 15:16:50 +0200

breeze-gtk (5.8.1-1) unstable; urgency=medium

  * New upstream release (5.8.1).

 -- Maximiliano Curia <maxy@debian.org>  Sun, 16 Oct 2016 22:54:31 +0200

breeze-gtk (5.8.0-1) unstable; urgency=medium

  * New upstream release (5.8.0).
  * Replace dbus-launch with dbus-run-session in tests

 -- Maximiliano Curia <maxy@debian.org>  Fri, 07 Oct 2016 14:00:26 +0200

breeze-gtk (5.8.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 30 Sep 2016 11:29:16 +0000

breeze-gtk (5.7.5-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 13 Sep 2016 13:45:24 +0000

breeze-gtk (5.7.4-1) unstable; urgency=medium

  * New upstream release (5.7.4).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 26 Aug 2016 14:45:46 +0200

breeze-gtk (5.7.4-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 23 Aug 2016 17:27:22 +0000

breeze-gtk (5.7.3-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 02 Aug 2016 12:04:05 +0000

breeze-gtk (5.7.2-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 19 Jul 2016 13:45:58 +0000

breeze-gtk (5.7.1-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 12 Jul 2016 16:06:03 +0000

breeze-gtk (5.7.0-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 04 Jul 2016 13:16:06 +0000

breeze-gtk (5.6.95-1) unstable; urgency=medium

  * Team upload.
  * New upstream beta release (5.6.95).
    - Drop all patches.
    - Fixes missing asset/ files.

 -- Felix Geyer <fgeyer@debian.org>  Thu, 23 Jun 2016 17:49:42 +0200

breeze-gtk (5.6.5-1) unstable; urgency=medium

  * Add upstream patch: upstream_update_for_gtk_3.20.patch
  * Add upstream patch: upstream_properly_install_theme.patch
  * Force the use of gtk 3.20

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Jun 2016 09:36:45 +0200

breeze-gtk (5.6.5-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 14 Jun 2016 14:15:35 +0000

breeze-gtk (5.6.4-1) unstable; urgency=medium

  [ Maximiliano Curia ]
  * New upstream release (5.5.5).
  * Add upstream metadata (DEP-12)
  * Drop testsuite dependency on experimental package libqt5xcbqpa5
  * debian/control: Update Vcs-Browser and Vcs-Git fields

  [ Automatic packaging ]
  * Bump Standards-Version to 3.9.8

 -- Maximiliano Curia <maxy@debian.org>  Thu, 26 May 2016 19:10:54 +0200

breeze-gtk (5.6.4-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 11 May 2016 09:41:29 +0000

breeze-gtk (5.6.3-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 19 Apr 2016 16:54:04 +0000

breeze-gtk (5.6.2-0neon) xenial; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 08 Apr 2016 11:43:55 +0000

breeze-gtk (5.5.4-1) experimental; urgency=medium

  * Initial release (5.5.0).
  * New upstream release (5.5.1).
  * New upstream release (5.5.2).
  * New upstream release (5.5.3).
  * New upstream release (5.5.4).

 -- Maximiliano Curia <maxy@debian.org>  Wed, 27 Jan 2016 16:48:48 +0100

breeze-gtk (5.5.4-0ubuntu1) xenial; urgency=medium

  [ Maximiliano Curia ]
  * Initial release (5.5.0).

  [ Clive Johnston ]
  * new upstream release
  * Adding build dependencies
  * Enabling xvfb-run for autotests
  * New upstream release
  * New upstream release

 -- Clive Johnston <clivejo@aol.com>  Mon, 07 Mar 2016 21:18:48 +0100
